# Libraries

import csv
import matplotlib.pyplot as plt

# Variables

pathOfCameraFile = "/Users/lml/Documents/lqdn/mappingetalgoantisurveillance/2018-11-15-export-cameras-pvpp.kml.csv"
pathOfMapBackground = "parisMap.png"

cameraPositions = [] # List is of the form [latitude,longitude]
allLongitudes = []
allLatitudes = []
maxLatitude = 0
minLatitude = 10000000
maxLongitude = 0
minLongitude = 10000000
eliminateFirstRow = 1 # Deal (poorly) with the header


# Begin

with open(pathOfCameraFile) as openedCsvFile:
	readedFile = csv.reader(openedCsvFile,delimiter=';')
	for row in readedFile:


		if eliminateFirstRow == 0:
			

			longitude = float(row[-3]) 
			latitude = float(row[-2]) 

			minLatitude = min(latitude,minLatitude)
			maxLatitude = max(latitude,maxLatitude)
			minLongitude = min(longitude,minLongitude)
			maxLongitude = max(longitude,maxLongitude)

			allLongitudes.append(longitude)
			allLatitudes.append(latitude)
			cameraPositions.append([latitude,longitude])

		else:
			eliminateFirstRow = 0

	boundingBox = (minLongitude,maxLongitude,minLatitude,maxLatitude)
	print (boundingBox)


	# plot the cameras
	fig, ax = plt.subplots()
	ax.scatter(allLongitudes, allLatitudes, color='red',s=0.8, marker = r'$\bullet$')
	plt.axis('off')

	imgBackground = plt.imread(pathOfMapBackground)
	ax.imshow(imgBackground, extent=[minLongitude,maxLongitude , minLatitude, maxLatitude])

	# show the map
	resolution_value = 1200
	plt.savefig("myImage.png", format="png", dpi=resolution_value)
	plt.show()

